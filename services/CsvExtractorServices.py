import csv
import unidecode
import random

class CsvExtractorService:
    def __init__(self):
        self.states = self._extractState()
        self.cities = self._extractCities()
        self.buildCities()
        self.buildStates()

    def _extractState(self):
        csv_file = open('data/estados.csv', encoding='utf-8')
        reader = csv.reader(csv_file)
        next(reader)
        return [{
            'id': item[0],
            'abbv': item[1],
            'name': item[2]
        } for item in reader]

    def _extractCities(self):
        csv_file = open('data/municipios.csv', encoding='utf-8')
        reader = csv.reader(csv_file)
        next(reader)
        return [{
            'id': item[0],    
            'name': item[1],
            'var': unidecode.unidecode(item[1]).lower()
        } for item in reader]

    def buildCities(self):
        file = open('data/cities.md', 'w', encoding='utf-8')
        file.write('## intent:cities\n')
        for city in self.cities:
            file.write(f"- [{city['name']}]{{entity: cities, value: '{city['var']}'}}\n")
        file.close()
        
    def buildStates(self):
        file = open('data/states.md', 'w', encoding='utf-8')
        file.write('## intent:states\n')
        for state in self.states:
            file.write(f"- [{state['name']}]{{entity: states, value: '{state['abbv']}'}}\n")
        file.close()

    def getRandomCity(self):
        return self.cities[random.randint(0, len(self.cities) - 1)]