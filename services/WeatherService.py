import requests
import xmltodict

class WeatherService:
    def __init__(self):
        self.API_URL = 'http://servicos.cptec.inpe.br'
        self.conditions = {
            'ec': 'Encoberto com Chuvas Isoladas',
            'ci': 'Chuvas Isoladas',
            'c': 'Chuva',
            'in': 'Instável',
            'pp': 'Poss. de Pancadas de Chuva',
            'cm': 'Chuva pela Manhã',
            'cn': 'Chuva a Noite',
            'pt': 'Pancadas de Chuva a Tarde',
            'pm': 'Pancadas de Chuva pela Manhã',
            'np': 'Nublado e Pancadas de Chuva',
            'pc': 'Pancadas de Chuva',
            'pn': 'Parcialmente Nublado',
            'cv': 'Chuvisco',
            'ch': 'Chuvoso',
            't': 'Tempestade',
            'ps': 'Predomínio de Sol',
            'e': 'Encoberto',
            'n': 'Nublado',
            'cl': 'Céu Claro',
            'nv': 'Nevoeiro',
            'g': 'Geada',
            'ne': 'Neve',
            'nd': 'Não Definido',
            'pnt': 'Pancadas de Chuva a Noite',
            'psc': 'Possibilidade de Chuva',
            'pcm': 'Possibilidade de Chuva pela Manhã',
            'pct': 'Possibilidade de Chuva a Tarde',
            'pcn': 'Possibilidade de Chuva a Noite',
            'npt': 'Nublado com Pancadas a Tarde',
            'npn': 'Nublado com Pancadas a Noite',
            'ncn': 'Nublado com Poss. de Chuva a Noite',
            'nct': 'Nublado com Poss. de Chuva a Tarde',
            'ncm': 'Nublado com Poss. de Chuva pela Manhã',
            'npm': 'Nublado com Pancadas pela Manhã',
            'npp': 'Nublado com Possibilidade de Chuva',
            'vn': 'Variação de Nebulosidade',
            'ct': 'Chuva a Tarde',
            'ppn': 'Poss. de Panc. de Chuva a Noite',
            'ppt': 'Poss. de Panc. de Chuva a Tarde',
            'ppm': 'Poss. de Panc. de Chuva pela Manhã'
        }

    #Buscar uma localidade (cidade)
    def searchLocation(self, text):
        data = []
        response = requests.get(f'{self.API_URL}/XML/listaCidades', {
            "city": text or ''
        })
        if response.status_code == 200:
            data = xmltodict.parse(response.content, encoding='ISO-8859-1')
            data = data['cidades']['cidade'] if data['cidades'] else []
            data = [dict(item) for item in data] if isinstance(data, list) else dict(data)
        return self._sendResponse(response.status_code, data)

    #Retorna a previsão do tempo para a próxima semana para uma cidade
    def nextWeekWeather(self, city_id):
        if city_id:
            response = requests.get(f'{self.API_URL}/XML/cidade/7dias/{city_id}/previsao.xml')
            data = {}
            if response.status_code == 200:
                content = dict(xmltodict.parse(response.content)['cidade'])
                data = content.copy()
                data['previsao'] = []
                for item in content['previsao']:
                    item['tempo'] = self.conditions.get(item['tempo'])
                    item = dict(item)
                    data['previsao'].append(item)
            return self._sendResponse(response.status_code, data)
        else:
            return self._sendResponse(400)

    #Padroniza a resposta da requisição
    def _sendResponse(self, status_code, data={}):
        error = False
        if status_code >= 400:
            error = True
        return {
            "error": error,
            "statusCode": status_code,
            "data": data
        }